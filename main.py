import requests
import os
import csv
import json
from datetime import datetime


def collect_data():
    url = "https://www.lifetime.plus/api/analysis2"
    headers = {
        "Accept": "*/*",
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:120.0) Gecko/20100101 Firefox/120.0"
    }

    t_date = datetime.now().strftime("%d_%m_%Y")

    response = requests.get(url=url, headers=headers)

    try:
        categories = response.json()["categories"]
    except:
        print(response.json())
        exit(0)
    result = []

    for c in categories:
        c_name = c.get("name").strip()
        c_items = c.get("items")

        for item in c_items:
            item_name = item.get("name").strip()
            item_price = item.get("price")
            item_descr = item.get("description").strip()
            item_wf = item.get("days")
            item_bio = item.get("biomaterial")

            result.append(
                [c_name, item_name, item_bio, item_descr, item_price, item_wf]
            )
    
    with open(f"result_{t_date}.csv", "a") as file:
        writer = csv.writer(file)

        writer.writerow(
            (
                "Категория",
                "Анализ",
                "Биоматериал",
                "Описание",
                "Стоимость",
                "Готовность дней"
            )
        )

        writer.writerows(
            result
        )


def main():
    collect_data()


if __name__ == "__main__":
    main()
